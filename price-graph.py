# -*- coding: utf-8 -*-

import numpy as np
from matplotlib import pyplot as plt


hire_price_per_session = 12
storage_price_per_week = 7
bike_price = 900
total_weeks = 104


def plotPrices():
    weeks = np.arange(total_weeks)
    for sessions_per_week in np.arange(1, 3, 0.5):
        cost = sessions_per_week*hire_price_per_session*weeks
        plt.plot(weeks, cost)
    # for sessions_per_week in np.arange(1, 3, 0.5):
    #     cost = sessions_per_week*hire_price_per_session*weeks -
    #            storage_price_per_week*weeks
    #     plt.plot(weeks, cost)
    plt.plot(weeks, [bike_price for i in range(total_weeks)])
    plt.show()


def plot(price):
    sessions_per_week = np.arange(0.5, 3.5, 0.25)
    time_saving = price/(sessions_per_week*hire_price_per_session)
    plt.plot(sessions_per_week, time_saving,
             label=u"Bike price = £%d" % (price))
    plt.ylabel("No. of weeks to until bike is paid off")
    plt.xlabel("No. of sessions each week")
    plt.xlim(0, 3.5)
    plt.ylim(0, 180)


def main():
    prices = [600, 800, 900, 1000]
    [plot(i) for i in prices]
    plt.legend()


if __name__ == '__main__':
    main()
    plt.show()

#import numpy as np
import json
from matplotlib import pyplot as plt
from collections import Counter


def addComponent():
    data_file = ""
    component_type = raw_input("Component type: ")
    data_file = component_type + ".json"
    with open(data_file, "r") as read_file:
        data = json.load(read_file)
    component = {}
    component["brand"] = raw_input("Brand: ")
    component["model"] = raw_input("Model: ")
    if (component_type != "tyre"):
        component["material"] = raw_input("Material: ")
    else:
        component["type"] = raw_input("Tyre type: ")
    component["weight"] = float(raw_input("Weight (kg): "))
    if (component_type == "wheel"):
        component["rim depth"] = float(raw_input("Rim depth (mm): "))
        component["tyre type"] = raw_input("Tyre type: ")
    component["price"] = float(raw_input("Price: "))
    data.append(component)
    with open(data_file, "w") as write_file:
        json.dump(data, write_file)


def readComponents(component_type):
    data_file = component_type + ".json"
    with open(data_file, "r") as read_file:
        data = json.load(read_file)
    print json.dumps(data, sort_keys=True, indent=4, separators=(',', ': '))


def generatePriceList():
    bikes = []
    with open("frame.json", "r") as read_file:
        data_frame = json.load(read_file)
    with open("wheel.json", "r") as read_file:
        data_wheel = json.load(read_file)
    with open("crank.json", "r") as read_file:
        data_crank = json.load(read_file)
    with open("handlebar.json", "r") as read_file:
        data_handlebar = json.load(read_file)
    with open("tyre.json", "r") as read_file:
        data_tyre = json.load(read_file)
    with open("saddle.json", "r") as read_file:
        data_saddle = json.load(read_file)
    with open("stem.json", "r") as read_file:
        data_stem = json.load(read_file)
    with open("chain.json", "r") as read_file:
        data_chain = json.load(read_file)
    for frame in data_frame:
        for wheel in data_wheel:
            for crank in data_crank:
                for handlebar in data_handlebar:
                    for saddle in data_saddle:
                        for stem in data_stem:
                            for chain in data_chain:
                                for tyre in data_tyre:
                                    bike = {}
                                    bike["frame"] = frame
                                    bike["total weight"] = frame["weight"]
                                    bike["total price"] = frame["price"]
                                    bike["wheel"] = wheel
                                    bike["total weight"] += wheel["weight"]
                                    bike["total price"] += wheel["price"]
                                    bike["crank"] = crank
                                    bike["total weight"] += crank["weight"]
                                    bike["total price"] += crank["price"]
                                    bike["handlebar"] = handlebar
                                    bike["total weight"] += handlebar["weight"]
                                    bike["total price"] += handlebar["price"]
                                    bike["saddle"] = saddle
                                    bike["total weight"] += saddle["weight"]
                                    bike["total price"] += saddle["price"]
                                    bike["stem"] = stem
                                    bike["total weight"] += stem["weight"]
                                    bike["total price"] += stem["price"]
                                    bike["chain"] = chain
                                    bike["total weight"] += chain["weight"]
                                    bike["total price"] += chain["price"]
                                    bike["tyre"] = tyre
                                    bike["total weight"] += tyre["weight"]
                                    bike["total price"] += tyre["price"]
                                    if (tyre["type"] == wheel["tyre type"]):
                                        bikes.append(bike)
    with open("bike.json", "w") as write_file:
        json.dump(bikes, write_file)


def printBike(bike):
    print "%s %s | %s %s | %s %s | %s %s | %s %s | %s %s | Total weight: %.02f | Total price: %.02f"%( bike["frame"]["brand"], bike["frame"]["model"],
                                                                                        bike["wheel"]["brand"], bike["wheel"]["model"],
                                                                                        bike["crank"]["brand"], bike["crank"]["model"],
                                                                                        bike["handlebar"]["brand"], bike["handlebar"]["model"],
                                                                                        bike["tyre"]["brand"], bike["tyre"]["model"],
                                                                                        bike["saddle"]["brand"], bike["saddle"]["model"],
                                                                                        bike["total weight"],
                                                                                        bike["total price"])


def printPriceList():
    with open("bike.json", "r") as read_file:
        data = json.load(read_file)
    for i in data:
        if (i["total price"] <= 899.99 and i["frame"]["model"] == "TC1" and i["wheel"]["brand"] == "Mavic"):# and i["wheel"]["brand"] != "Miche"):
            printBike(i)
    sub_1000 = [i["wheel"]["model"] for i in data if (i["total price"] < 1000 and i["frame"]["model"] == "TC1")]
    print Counter(sub_1000)
    print len(data)


def plotPrices():
    with open("bike.json", "r") as read_file:
        data = json.load(read_file)
    prices = [i["total price"] for i in data]# if (i["frame"]["model"] == "TC1" and i["wheel"]["model"] == "Pro Carbon" and i["crank"]["model"] == "RD2")]
    plt.hist(prices, 50)
    plt.xlabel(u'Total price (\u00a3)')
    plt.ylabel('Quantity')
    plt.show()


def main():
    if (raw_input("Add component? : ") == "y"):
        addComponent()
    #generatePriceList()
    printPriceList()
    plotPrices()


if (__name__ == '__main__'):
    main()

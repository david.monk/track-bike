from __future__ import division
import sys
from numpy import pi
from matplotlib import pyplot as plt


wheel_size = 622  # mm
tyre_width = 23  # mm
mm_in_inch = 25.4
wheel_diameter = (wheel_size + 2*tyre_width)/mm_in_inch
km_in_mile = 1.60934


def calcInches(chainring, sprocket):
    return wheel_diameter*chainring/sprocket


def calcSpeed(inches, target_cadence):
    return inches * target_cadence * pi/1056 * km_in_mile


def calcCadence(inches, target_speed):
    return target_speed / (inches * pi/1056 * km_in_mile)


def getStatistics(chainring, sprocket, target_cadence, target_speed):
    inches = calcInches(chainring, sprocket)
    speed = calcSpeed(inches, target_cadence)
    cadence = calcCadence(inches, target_speed)
    text = "%d %d : Inches = %0.1f | " % (chainring, sprocket, inches) + \
           "Speed at %drpm = %0.1f | " % (target_cadence, speed) + \
           "Cadence at %dkph = %0.1f" % (target_speed, cadence)
    return text


def plotInches(chainrings, sprockets):
    sorted(chainrings)
    sorted(sprockets)
    inches = []
    for i in range(len(chainrings)):
        inches.append([])
        for sprocket in sprockets:
            inches[i].append(calcInches(chainrings[i], sprocket))
    plt.imshow(inches, interpolation='none',
               extent=[min(sprockets), max(sprockets),
                       max(chainrings), min(chainrings)])
    plt.colorbar()
    plt.show()


def main():
    target_cadence = 90
    target_speed = 45
    if len(sys.argv) >= 3:
        chainring = int(sys.argv[1])
        sprocket = int(sys.argv[2])
        if len(sys.argv) >= 5:
            target_cadence = int(sys.argv[3])
            target_speed = int(sys.argv[4])
    else:
        chainring = int(raw_input("No. of teeth on chainring: "))
        sprocket = int(raw_input("No. of teeth on sprocket: "))
    print getStatistics(chainring, sprocket, target_cadence, target_speed)
    plotInches([48, 49, 50, 51, 52], [13, 14, 15, 16])


if __name__ == '__main__':
    main()
